var cacheName = 'weatherPWA-v-1-1';
var filesToCache = [];

self.addEventListener('install', function(e) {
  console.log('[ServiceWorker] Install');
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      console.log('[ServiceWorker] Caching app shell');
        var filesToCache = [
        '/',
        //------CONTENU----------    
        '/index.html',
        '/contact.php',
        //------JS----------  
        '/js/init.js',
        '/js/index.js',
        '/js/jquery-2.1.1.min.js',
        '/js/materialize.js',
        '/js/materialize.min.js',
        '/js/modernizr.js',
        //------CSS----------      
        '/css/style.css',
        '/css/font-awesome.min.css',
        '/css/materialize.css',
        '/css/materialize.min.css',
        //------IMAGES----------      
        '/img/avatar1.png',
        '/img/project1.jpg',
        '/img/project2.jpeg',
        '/img/project3.png',
        '/img/project4.jpg',
        '/img/project5.png',
        '/img/project6.jpeg',
        '/img/parallax1.jpg',
        '/img/parallax2.jpg',
        '/img/parallax3.jpg',
        '/img/parallax4.jpg',
        '/img/topBg.jpeg',
        //------MIN----------      
        '/min/custom-min.css',
        '/min/custom-min.js',
        '/min/plugin-min.css',
        '/min/plugin-min.js',
        ];
      return cache.addAll(filesToCache);
    })
  );
});

self.addEventListener('activate', function(e) {
console.log('[ServiceWorker] Activate');
e.waitUntil(
caches.keys().then(function(keyList) {
  return Promise.all(keyList.map(function(key) {
    if (key !== cacheName) {
      console.log('[ServiceWorker] Removing old cache', key);
      return caches.delete(key);
    }
  }));
})
);
return self.clients.claim();
});

self.addEventListener('fetch', function(e) {
  console.log('[ServiceWorker] Fetch', e.request.url);
  e.respondWith(
    caches.match(e.request).then(function(response) {
      return response || fetch(e.request);
    })
  );
});
